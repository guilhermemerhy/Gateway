﻿using Flunt.Notifications;
using Flunt.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gateway.Domain.Validations
{
    public class CreateOrderCommandValidation : Notifiable
    {

        public void Validate(CreateOrderCommand command)
        {
            if (command.Type != "CreditCard")
                AddNotification("Type", "A transação deve do tipo CreditCard");

            if (command.Amount == 0)
                AddNotification("Amount", "Amount deve ser maior que zero");
        }
    }
}
