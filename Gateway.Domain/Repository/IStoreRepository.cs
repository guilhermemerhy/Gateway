﻿using Gateway.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gateway.Domain.Repository
{
    public interface IStoreRepository
    {
        Task<Store> FindAsync(string id);

        Task<IList<Store>> FindAllAsync();

        Task InsertManyAsync(IList<Store> stores);
    }
}
