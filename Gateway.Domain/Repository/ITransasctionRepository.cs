﻿using Gateway.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Gateway.Domain.Repository
{
    public interface ITransasctionRepository
    {
        Task InsertAsync(Transaction transaction);

        Task<IList<Transaction>> GetAllByStoreIdAsync(string storeId);

    }
}
