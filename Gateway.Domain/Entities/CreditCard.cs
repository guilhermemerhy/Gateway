﻿using Flunt.Notifications;
using Flunt.Validations;
using System;
using System.Text;

namespace Gateway.Domain.Entities
{
    public class CreditCard : Notifiable
    {
        public CreditCard(string cardNumber, string brand, int expMonth, int expYear, string holder, string cvv)
        {
            CardNumber = cardNumber;
            Brand = brand;
            ExpMonth = expMonth;
            ExpYear = expYear;
            Holder = holder;
            Cvv = cvv;
        }

    

        public string CardNumber { get; private set; }

        public string Brand { get; private set; }

        public int ExpMonth { get; private set; }

        public int ExpYear { get; private set; }

        public string Holder { get; private set; }

        public string Cvv { get; private set; }

        public bool IsValid()
        {
            AddNotifications(new Contract()
                .IsNotNullOrEmpty(CardNumber, "CardNumber", "CardNumber deve ser informado")
                .IsNotNullOrEmpty(Brand, "Brand", "Brand deve ser informado")
                .IsNotNullOrEmpty(Holder, "Holder", "Holder deve ser informado")
                .IsNotNullOrEmpty(Cvv, "Cvv", "Cvv deve ser informado")
                );

            if (DateTime.Now.Year > ExpYear)
                AddNotification("ExpYear", "ExpYear não pode ser menor que o ano atual");

            if(ExpYear == DateTime.Now.Year && ExpMonth < DateTime.Now.Month)
                AddNotification("ExpMonth", "ExpMonth não pode ser menor que do ano atual");

            return Valid;

        }
    }
}
