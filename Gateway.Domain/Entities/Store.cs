﻿using System;
using System.Collections.Generic;


namespace Gateway.Domain.Entities
{
    public class Store
    {
        public Store(string Id, string name, bool antiFraud, List<Acquirer> acquirer)
        {
            _id = Id;
            Name = name;
            AntiFraud = antiFraud;
            Acquirer = acquirer;
        }
        
        public string _id { get; private set; }

        public string Name { get; private set; }

        public bool AntiFraud { get; private set; }

        public List<Acquirer> Acquirer { get; private set; }


        public string GetAcquirer(string cardBrand) 
            => Acquirer.Find(x => string.Equals(x.CreditCardBrand, cardBrand, StringComparison.OrdinalIgnoreCase))?.Name;

        public bool IsValid() 
            => !(string.IsNullOrEmpty(_id) || string.IsNullOrEmpty(Name) || Acquirer.Count == 0);

        public bool AcquirerIsValid()
        {
            foreach (var item in Acquirer)
                if (!item.IsValid())
                    return false;

            return true;
        }

    }
}
