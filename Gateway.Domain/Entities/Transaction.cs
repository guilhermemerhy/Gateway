﻿using System;

namespace Gateway.Domain.Entities
{
    public class Transaction
    {

        public Transaction(string id, Store store, string acquirerName, CreditCard creditCard, DateTime create, string status, bool success, string payment_method, int installments)
        {
            _id = id;
            this.Store = store;
            AcquirerName = acquirerName;
            this.CreditCard = creditCard;
            Create = create;
            Status = status;
            Success = success;
            Payment_method = payment_method;
            Installments = installments;
        }

        public string _id { get; private set; }
    
        public Store Store { get; private set; }

        public string AcquirerName { get; private set; }

        public CreditCard CreditCard { get; set; }

        public DateTime Create { get; set; }

        public string Status { get; set; }

        public bool Success { get; set; }

        public string Payment_method { get; set; }

        public int Installments { get; private set; }

        public bool IsValid()
        {
            return !(string.IsNullOrEmpty(_id) ||
                string.IsNullOrEmpty(AcquirerName) ||
                string.IsNullOrEmpty(Payment_method) ||
                string.IsNullOrEmpty(Status) 
            );
        }
    }
}
