﻿namespace Gateway.Domain.Entities
{
    public class Acquirer
    {
        public Acquirer(string name, string creditCardBrand)
        {
            Name = name;
            CreditCardBrand = creditCardBrand;            
        }

        public string Name { get; private set; }

        public string CreditCardBrand { get; private set; }

        public bool IsValid() => !(string.IsNullOrEmpty(CreditCardBrand) || string.IsNullOrEmpty(Name));
    }
}
