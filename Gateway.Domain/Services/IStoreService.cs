﻿using Gateway.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gateway.Domain.Services
{
    public interface IStoreService
    {
        Task<bool> CreateAsync();

        Task<IList<Store>> GetAllAsync();
    }
}
