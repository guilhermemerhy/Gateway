﻿using Gateway.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gateway.Domain.Services
{
    public interface ITransactionService
    {
        Task<bool> CreateAsync(CreateOrderCommand command);

        Task<IList<Transaction>> GetTransactionsByStoreAsync(string storeId);
    }
}
