﻿using System.Threading.Tasks;

namespace Gateway.Domain.Services
{
    public interface IAntiFraudService
    {
        Task<bool> CheckAntiFraudAsync();
    }
}
