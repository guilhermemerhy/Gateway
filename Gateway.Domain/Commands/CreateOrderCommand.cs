﻿
using Flunt.Notifications;
using Gateway.Domain.Abstractions;
using Gateway.Domain.Validations;

namespace Gateway.Domain
{
    public class CreateOrderCommand : Notifiable, ICommand
    {
        public string IdStore { get; set; }

        public string NameStore { get; set; }

        public string Type { get; set; }

        public double Amount { get; set; }

        public int Installments { get; set; }

        public string CardNumber { get; set; }

        public int ExpMonth { get; set; }

        public int ExpYear { get; set; }

        public string Brand { get; set; }

        public string Holder { get; set; }

        public string Cvv { get; set; }

        public string Email { get; set; }

        public bool IsValid()
        {            
           var command =  new CreateOrderCommandValidation();

            command.Validate(this);

            AddNotifications(command.Notifications);

            return Valid;
        }
    }
}
