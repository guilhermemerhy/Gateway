﻿using Autofac;
using Gateway.Domain.Services;
using Gateway.Facade.Service;

namespace Gateway.Facade.DependencyInjection
{
    public static class FacadeExtensions
    {
        public static void AddFacade(this ContainerBuilder builder)
        {
            builder.RegisterType<TransactionService>().As<ITransactionService>();
            builder.RegisterType<StoreService>().As<IStoreService>();
        }
    }
}
