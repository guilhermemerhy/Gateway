﻿using Gateway.Domain.Entities;
using Gateway.Domain.Repository;
using Gateway.Domain.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gateway.Facade.Service
{
    public class StoreService : IStoreService
    {
        private readonly IStoreRepository _storeRepository;

        public StoreService(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository;
        }

        public async Task<bool> CreateAsync()
        {
            var randon = new Random();

            var lsAcquirer = new List<Acquirer>
            {
                new Acquirer("Cielo", "Visa"),
                new Acquirer("Stone", "Mastercard")

             };

            

            var lsAcquirer2 = new List<Acquirer>
            {
                new Acquirer("Stone", "Visa"),
                new Acquirer("Cielo", "Mastercard")

             };

            var lsStore = new List<Store> {
                 new Store(randon.Next(999999999).ToString("D9"), "Fake1", true, lsAcquirer),
                 new Store(randon.Next(999999999).ToString("D9"), "Fake2", false, lsAcquirer),
                 new Store(randon.Next(999999999).ToString("D9"), "Fake3", true, lsAcquirer2),
             };


        
            foreach (var store in lsStore)
                if (!store.IsValid() || !store.AcquirerIsValid())
                    return false;


            await _storeRepository.InsertManyAsync(lsStore);

            return true;
        }

        public async Task<IList<Store>> GetAllAsync() => await _storeRepository.FindAllAsync();        
    }
}
