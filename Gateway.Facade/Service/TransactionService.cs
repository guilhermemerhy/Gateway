﻿using Gateway.Domain;
using Gateway.Domain.Cielo;
using Gateway.Domain.Cielo.Converters;
using Gateway.Domain.Entities;
using Gateway.Domain.Repository;
using Gateway.Domain.Services;
using Gateway.Domain.Stone;
using Gateway.Domain.Stone.Converter;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gateway.Facade.Service
{
    public class TransactionService : ITransactionService
    {
        private readonly IStoreRepository _storeRepository;
        private readonly ITransasctionRepository _transactionRepository;
        readonly IAntiFraudService _antiFraudService;
        private readonly CieloPurchase _cieloPurchase;
        private readonly StonePurchase _stonePurchase;
        private readonly NotificationContext _notificationContext;


        public TransactionService(IStoreRepository storeRepository, ITransasctionRepository transactionRepository, IAntiFraudService antiFraudService, CieloPurchase cieloPurchase, StonePurchase stonePurchase, NotificationContext notificationContext)
        {
            _storeRepository = storeRepository;
            _transactionRepository = transactionRepository;
            _antiFraudService = antiFraudService;
            _cieloPurchase = cieloPurchase;
            _stonePurchase = stonePurchase;
            _notificationContext = notificationContext;
        }

        public async Task<bool> CreateAsync(CreateOrderCommand command)
        {
            if (!command.IsValid())
                _notificationContext.AddNotifications(command.Notifications);

            var creditCard = CreateCreditCard(command);

            if (!creditCard.IsValid())
                _notificationContext.AddNotifications(creditCard.Notifications);


            var store = await _storeRepository.FindAsync(command.IdStore);

            if (store == null)
                _notificationContext.AddNotification("Store", "Loja não encontrada");


            if (_notificationContext.HasNotifications)
                return false;


            if (!await VerfiyAntiFraud(store))
                return false;


            var acquirer = store.GetAcquirer(command.Brand);

            if (acquirer == "Cielo")
            {
                //Conversão do dados
                var converter = new CieloCreateOrderRequestConverter();

                //Envia transação  
                var response = await _cieloPurchase.PayAsync(converter.Convert(command));

                //Salvar transação
                await SaveTransaction(CreateTransaction(creditCard,store, acquirer, response.MerchantOrderId, DateTime.Now, response.Payment.ReturnMessage, response.Payment.ReturnCode == "4", response.Payment.Type, response.Payment.Installments));

            }
            else
            {
                //Conversão do dados
                var converter = new StoneCreateOrderRequestConverter();

                //Envia transação  
                var response = await _stonePurchase.PayAsync(converter.Convert(command));

                await SaveTransaction(CreateTransaction(creditCard, store, acquirer, response.Id, response.last_transaction.created_at, response.last_transaction.status, response.last_transaction.success, response.payment_method, command.Installments));
            }

            

            return true;
        }

        private static CreditCard CreateCreditCard(CreateOrderCommand command) 
            => new CreditCard(command.CardNumber, command.Brand, command.ExpMonth, command.ExpYear, command.Holder, command.Cvv);

        private static Transaction CreateTransaction(CreditCard creditCard, Store store, string acquirer,
                                                     string idTransaction, DateTime create,
                                                     string status, bool success, string paymenth_method,
                                                     int installments)
        {


            return new Transaction(idTransaction, store, acquirer, creditCard, create, status, success, paymenth_method, installments);
        }

        private async Task SaveTransaction(Transaction transaction)
        {
            await _transactionRepository.InsertAsync(transaction);
        }

        private async Task<bool> VerfiyAntiFraud(Store store)
        {
            return store.AntiFraud ? await _antiFraudService.CheckAntiFraudAsync() : true;
        }

        public async Task<IList<Transaction>> GetTransactionsByStoreAsync(string storeId) 
            => await _transactionRepository.GetAllByStoreIdAsync(storeId);
    }
}
