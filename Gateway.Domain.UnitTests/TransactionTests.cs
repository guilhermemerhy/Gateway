﻿using Gateway.Domain.Entities;
using System;
using System.Collections.Generic;
using Xunit;

namespace Gateway.Domain.UnitTests
{
    public class TransactionTests
    {

        [Fact]
        public void Transaction_Create_IsValid()
        {
            var random = new Random().Next(9999).ToString("D6");

            var creditCard = new CreditCard("293923910200292", "Visa", 12, 2018, "Holder", "123");
            
            var lsAcquirer = new List<Acquirer>
            {
                 new Acquirer("Cielo", "Visa")
            };

            var store = new Store("5555", "test", true, lsAcquirer);

            var transaction = new Transaction(random, store, "Cielo", creditCard, DateTime.Now, "ok", true, "credit_card", 1);
            Assert.True(transaction.IsValid());

        }

        [Fact]
        public void Transaction_Create_IsNotValid()
        {

            var creditCard = new CreditCard("293923910200292", "Visa", 12, 2018, "Holder", "123");

            var lsAcquirer = new List<Acquirer>
            {
                 new Acquirer("Cielo", "Visa")
            };

            var store = new Store("5555", "test", true, lsAcquirer);

            var transaction = new Transaction("", store, "Cielo", creditCard, DateTime.Now, "ok", true, "credit_card", 1);

            Assert.False(transaction.IsValid());

        }
    }
}
