﻿using Gateway.Domain.Entities;
using Xunit;

namespace Gateway.Domain.UnitTests
{
    public class AcquirerTests
    {
        [Fact]
        public void Acquirer_Create_IsValid()
        {

            var acquirer = new Acquirer("Cielo", "Visa");


            Assert.True(acquirer.IsValid());

        }

        [Fact]
        public void Acquirer_Create_IsNotValid()
        {
            var acquirer = new Acquirer("", "Visa");

            Assert.False(acquirer.IsValid());
        }
    }
}
