﻿using Gateway.Domain.Entities;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Gateway.Domain.UnitTests
{
    public class CreditCardTest
    {
        [Fact]
        public void Create_CreditCard_IsValid()
        {

            CreditCard actual = new CreditCard("293923910200292", "Visa", 12, 2019, "Holder", "123");

            Assert.True(actual.IsValid());
        }
        [Fact]
        public void Create_ExpiredCreditCard_Invalid()
        {
            CreditCard actual = new CreditCard("293923910200292", "Visa", 12, 2017, "Holder", "123");

            Assert.False(actual.IsValid());
        }

        [Fact]
        public void Create_CvvIsEmpty_Invalid()
        {
            CreditCard actual = new CreditCard("293923910200292", "Visa", 12, 2017, "Holder", "");

            Assert.False(actual.IsValid());
        }

        [Fact]
        public void Create_CardNumberIsEmpty_Invalid()
        {
            CreditCard actual = new CreditCard("", "Visa", 12, 2017, "Holder", "424");

            Assert.False(actual.IsValid());
        }
    }
}
