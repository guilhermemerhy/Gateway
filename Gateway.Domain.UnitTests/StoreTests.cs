﻿using Gateway.Domain.Entities;
using System.Collections.Generic;
using Xunit;

namespace Gateway.Domain.UnitTests
{
    public class StoreTests
    {

        [Fact]
        public void Transaction_Create_IsValid()
        {

            var lsAcquirer = new List<Acquirer>
            {
                new Acquirer("Cielo", "Visa"),
                new Acquirer("Stone", "Mastercars")

             };

            var store = new Store("84498498", "Fake1", true, lsAcquirer);

            Assert.True(store.IsValid());

        }

        [Fact]
        public void Transaction_Create_IsNotValid()
        {
            var lsAcquirer = new List<Acquirer>();

            var store = new Store("84498498", "Fake1", true, lsAcquirer);

            Assert.False(store.IsValid());            
        }
    }
}
