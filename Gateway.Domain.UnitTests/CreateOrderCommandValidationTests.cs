﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Gateway.Domain.UnitTests
{
    public class CreateOrderCommandValidationTests
    {
        [Fact]
        public void CreateOrderCommand_IsValid()
        {

            var command = new CreateOrderCommand
            {
                Amount = 10,
                CardNumber = "51561565",
                Brand = "Mastercard",
                Cvv = "555",
                Email = "teste@gmail.com",
                ExpYear = 2019,
                ExpMonth = 12,
                Holder = "tetse",
                IdStore = "565656",
                Installments = 1,
                NameStore = "Novo teste",
                Type = "CreditCard"
            };


            Assert.True(command.IsValid());

        }

        [Fact]
        public void Command_Type_IsNotValid()
        {
            var command = new CreateOrderCommand
            {
                Amount = 10,
                CardNumber = "51561565",
                Brand = "Mastercard",
                Cvv = "555",
                Email = "teste@gmail.com",
                ExpYear = 2019,
                ExpMonth = 12,
                Holder = "tetse",
                IdStore = "565656",
                Installments = 1,
                NameStore = "Novo teste",
                Type = "Invalido"
            };


            Assert.False(command.IsValid());

        }

        [Fact]
        public void Command_Amount_IsNotValid()
        {
            var command = new CreateOrderCommand
            {
                Amount = 0,
                CardNumber = "51561565",
                Brand = "Mastercard",
                Cvv = "555",
                Email = "teste@gmail.com",
                ExpYear = 2019,
                ExpMonth = 12,
                Holder = "tetse",
                IdStore = "565656",
                Installments = 1,
                NameStore = "Novo teste",
                Type = "CreditCard"
            };


            Assert.False(command.IsValid());

        }
    }
}
