﻿using Gateway.Domain.Repository;
using Gateway.Domain.Entities;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gateway.Infrastructure.Repository
{
    public class StoreRepository : IStoreRepository
    {
        readonly MongoDbContext _context;

        public StoreRepository(MongoDbContext context)
        {
            _context = context;

        }

        public async Task<IList<Store>> FindAllAsync()
        {
            return await _context.Store.FindAsync(_ => true).Result.ToListAsync();
        }

        public async Task<Store> FindAsync(string id)
        {
            var filter = Builders<Store>.Filter.Eq("_id", id);

           return await _context.Store.FindAsync(filter).Result.FirstOrDefaultAsync();
        }

        public async Task InsertManyAsync(IList<Store> stores)
        {
            await _context.Store.InsertManyAsync(stores);
        }
    }
}
