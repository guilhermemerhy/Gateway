﻿using Gateway.Domain.Entities;
using Gateway.Domain.Repository;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gateway.Infrastructure.Repository
{
    public class TransactionRepository : ITransasctionRepository
    {
        readonly MongoDbContext _context;

        public TransactionRepository(MongoDbContext context)
        {
            _context = context;

        }

        public async Task<IList<Transaction>> GetAllByStoreIdAsync(string storeId)
        {
            var filter = Builders<Transaction>.Filter.Eq(x => x.Store._id, storeId);

            return await _context.Transaction.FindAsync(filter).Result.ToListAsync();
        }

        public async Task InsertAsync(Transaction transaction)
        {
            await _context.Transaction.InsertOneAsync(transaction);
        }
    }
}
