﻿using Gateway.Domain.Entities;
using MongoDB.Driver;
using System;

namespace Gateway.Infrastructure
{
    public class MongoDbContext
    {
        public static string ConnectionString { get; set; }
        public static string DatabaseName { get; set; }        
        private IMongoDatabase _database { get; }
        public MongoDbContext()
        {
            try
            {
                var settings = MongoClientSettings.FromUrl(new MongoUrl(ConnectionString));
                var mongoClient = new MongoClient(settings);
                _database = mongoClient.GetDatabase(DatabaseName);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível se conectar com o servidor.", ex);
            }
        }

        public IMongoCollection<Store> Store
        {
            get
            {
                return _database.GetCollection<Store>("Store");
            }
        }

        public IMongoCollection<Transaction> Transaction
        {
            get
            {
                return _database.GetCollection<Transaction>("Transaction");
            }
        }
    }
}
