﻿using Autofac;
using Gateway.Domain.Repository;
using Gateway.Infrastructure.Repository;

namespace Gateway.Infrastructure.DependencyInjection
{
    public static class RepositoryExtensions
    {
        public static void AddRepository(this ContainerBuilder builder)
        {
            builder.RegisterType<MongoDbContext>().As<MongoDbContext>();
            builder.RegisterType<StoreRepository>().As<IStoreRepository>();
            builder.RegisterType<TransactionRepository>().As<ITransasctionRepository>();
        }
    }
}
