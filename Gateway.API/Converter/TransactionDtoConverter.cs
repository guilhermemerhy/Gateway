﻿using Gateway.API.Dtos;
using Gateway.Domain.Abstractions;
using Gateway.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.API.Converter
{
    public class TransactionDtoConverter : IConverterObjects<Transaction, TransactionDto>
    {
        public TransactionDto Convert(Transaction from)
        {
            return new TransactionDto
            {
                AcquirerName = from.AcquirerName,
                CardNumber = from.CreditCard.CardNumber,
                Brand = from.CreditCard.Brand,
                Installments = from.Installments,
                create = from.Create,
                payment_method = from.Payment_method,
                NameStore = from.Store.Name,
                status = from.Status,
                success = from.Success,
                _id = from._id,
                
            };
        }
    }
}
