﻿using System;

namespace Gateway.API.Dtos
{
    public class TransactionDto
    {

        public string _id { get; set; }

        public string NameStore { get; set; }

        public string AcquirerName { get; set; }

        public string Brand { get; set; }

        public DateTime create { get; set; }

        public string status { get; set; }

        public bool success { get; set; }

        public string payment_method { get; set; }

        public int Installments { get; set; }

        public string CardNumber { get; set; }
    }
}
