﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gateway.API.Converter;
using Gateway.API.Dtos;
using Gateway.Domain;
using Gateway.Domain.Repository;
using Gateway.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Gateway.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {

        private readonly ITransactionService _transactionService;

        private readonly NotificationContext _notificationContext;

        public TransactionController(ITransactionService transactionService, NotificationContext notificationContext)
        {
            _transactionService = transactionService;
            _notificationContext = notificationContext;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Post([FromBody] CreateOrderCommand command)
        {

            if (await _transactionService.CreateAsync(command))
                return StatusCode(201);

            return BadRequest(new
            {
                notifications = _notificationContext.Notifications
            });

        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(TransactionDto))]        
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get(string id)
        {
            var result = await _transactionService.GetTransactionsByStoreAsync(id);

            var transactionDtoList = new List<TransactionDto>();
            var convert = new TransactionDtoConverter();

            foreach (var item in result)
                transactionDtoList.Add(convert.Convert(item));

            return Ok(transactionDtoList);
        }
    }
}