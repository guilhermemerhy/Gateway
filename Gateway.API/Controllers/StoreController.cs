﻿using System.Threading.Tasks;
using Gateway.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace Gateway.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreController : ControllerBase
    {

        private readonly IStoreService _storeService;

        public StoreController(IStoreService storeService)
        {
            _storeService = storeService;
        }


        [HttpPost]        
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Post()
        {                     
            return await _storeService.CreateAsync() ? StatusCode(201) : BadRequest();
        }

        [HttpGet]
        [ProducesResponseType(200)]        
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _storeService.GetAllAsync());
        }
    }
}