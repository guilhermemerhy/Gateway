﻿using Autofac;
using Gateway.AntiFraud.DependencyInjection;
using Gateway.Domain;
using Gateway.Domain.Cielo.DependencyInjection;
using Gateway.Domain.Stone.DependencyInjection;
using Gateway.Facade.DependencyInjection;
using Gateway.Infrastructure;
using Gateway.Infrastructure.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Gateway.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            MongoDbContext.ConnectionString = Configuration.GetSection("MongoConnection:ConnectionString").Value;
            MongoDbContext.DatabaseName = Configuration.GetSection("MongoConnection:Database").Value;

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterType<NotificationContext>().As<NotificationContext>().InstancePerLifetimeScope();
            builder.AddRepository();            
            builder.AddAntiFraud();
            builder.AddCieloGateway();
            builder.AddStoneGateway();
            builder.AddFacade();

        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
