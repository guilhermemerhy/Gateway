﻿using Autofac;

namespace Gateway.Domain.Stone.DependencyInjection
{
    public static class StoneExtensions
    {
        public static void AddStoneGateway(this ContainerBuilder builder)
        {
            builder.RegisterType<StonePurchase>().As<StonePurchase>();
        }
    }
}
