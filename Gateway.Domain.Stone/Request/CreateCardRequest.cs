﻿namespace Gateway.Domain.Stone.Request
{
    public class CreateCardRequest
    {
        public string Number { get; set; }

        public string HolderName { get; set; }

        public int ExpYear { get; set; }

        public int ExpMonth { get; set; }

        public string Cvv { get; set; }
    }
}
