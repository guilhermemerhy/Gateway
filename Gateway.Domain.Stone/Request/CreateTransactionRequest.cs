﻿using Gateway.Domain.Abstractions;

namespace Gateway.Domain.Stone.Request
{
    public class CreateTransactionRequest : ICommand
    {
        public double amount { get; set; }

        public CreatePaymentRequest payment { get; set; }

        public CreateCustomerRequest Customer { get; set; }
        
    }
}
