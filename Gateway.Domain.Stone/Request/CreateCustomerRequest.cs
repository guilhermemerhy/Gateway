﻿namespace Gateway.Domain.Stone.Request
{
    public class CreateCustomerRequest
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
