﻿namespace Gateway.Domain.Stone.Request
{
    public class CreateCreditCardPaymentRequest
    {
        public int installments { get; set; }

        public CreateCardRequest Card { get; set; }
    }
}
