﻿namespace Gateway.Domain.Stone.Request
{
    public class CreatePaymentRequest
    {
        public string PaymentMethod { get; set; }

        public CreateCreditCardPaymentRequest credit_card { get; set; }

        public MetaDataRequest metadata { get; set; }
    }
}
