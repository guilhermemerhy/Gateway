﻿using System;

namespace Gateway.Domain.Stone.Response
{
    public class LastTransactionResponse
    {
        public string status { get; set; }

        public bool success { get; set; }
        
        public DateTime created_at { get; set; }

        public GatewayResponse gateway_response { get; set; }
    }
}
