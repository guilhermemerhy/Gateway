﻿using Gateway.Domain.Abstractions;

namespace Gateway.Domain.Stone.Response
{
    public class TransactionResponse : ICommandResult
    {
        public string Id { get; set; }

        public string status { get; set; }

        public string payment_method { get; set; }

        public string currency { get; set; }

        public LastTransactionResponse last_transaction { get; set; }
    }
}
