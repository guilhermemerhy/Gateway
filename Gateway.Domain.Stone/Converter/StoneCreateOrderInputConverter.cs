﻿using Gateway.Domain.Abstractions;
using Gateway.Domain.Stone.Request;

namespace Gateway.Domain.Stone.Converter
{
    public class StoneCreateOrderRequestConverter : IConverterObjects<CreateOrderCommand, CreateTransactionRequest>
    {
        public CreateTransactionRequest Convert(CreateOrderCommand from)
        {
            return new CreateTransactionRequest
            {
                Customer = new CreateCustomerRequest
                {
                    Name = from.NameStore,
                    Email = from.Email,
                },
                amount = from.Amount,
                payment = new CreatePaymentRequest
                {
                    metadata = new MetaDataRequest
                    {
                        mundipagg_payment_method_code = "1"
                    },

                    PaymentMethod = from.Type,

                    credit_card = new CreateCreditCardPaymentRequest
                    {
                        Card = new CreateCardRequest
                        {
                            Number = from.CardNumber,
                            HolderName = from.Holder,
                            ExpMonth = from.ExpMonth,
                            ExpYear = from.ExpYear,
                            Cvv = from.Cvv,
                        },

                        installments = from.Installments
                    }
                }
            };

        }
    }

}
