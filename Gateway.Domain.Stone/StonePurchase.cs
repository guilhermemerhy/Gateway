﻿using Gateway.Domain.Abstractions;
using Gateway.Domain.Stone.Request;
using Gateway.Domain.Stone.Response;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Gateway.Domain.Stone
{
    public class StonePurchase : IPurchaseTransaction<CreateTransactionRequest, TransactionResponse>
    {

        public async Task<TransactionResponse> PayAsync(CreateTransactionRequest client)
        {
            var values = Enum.GetValues(typeof(HttpStatusCode));
            Random random = new Random();
            var randomBar = (HttpStatusCode)values.GetValue(random.Next(3));

            //Retornar Moke resposta
            return new TransactionResponse
            {
                currency = "BRL",
                Id = random.Next(0, 999999).ToString("D6"),
                payment_method = client.payment.PaymentMethod,
                status = "paid",
                last_transaction = new LastTransactionResponse
                {
                    status = randomBar == HttpStatusCode.Created ? "captured": "failed",
                    success = randomBar == HttpStatusCode.Created,
                    created_at = DateTime.Now,
                    
                    gateway_response = new GatewayResponse
                    {
                        code = randomBar.ToString()
                    },    
                }

            };
        }
    }
}
