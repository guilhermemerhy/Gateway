using Gateway.AntiFraud.Response;
using Gateway.Domain.Enums;
using System;
using Xunit;

namespace Gateway.AntiFraud.UnitTests
{
    public class AntiFraudTest
    {
        [Fact]
        public void AntiFraud_IsValid()
        {
            Random random = new Random();
            var test = new AntiFraudResponse
            {
                TransactionID = random.Next(0, 999999).ToString("D6"),
                Orders = new OrderStatusResponse
                {
                    ID = random.Next(0, 999999).ToString("D6"),
                    Status = OrderStatus.APA.ToString()
                }
            };

            Assert.Equal(OrderStatus.APA.ToString(), test.Orders.Status);

        }

        [Fact]
        public void AntiFraud_IsNotValid()
        {
            Random random = new Random();
            var test = new AntiFraudResponse
            {
                TransactionID = random.Next(0, 999999).ToString("D6"),
                Orders = new OrderStatusResponse
                {
                    ID = random.Next(0, 999999).ToString("D6"),
                    Status = OrderStatus.APM.ToString()
                }
            };

            Assert.NotEqual(OrderStatus.APA.ToString(), test.Orders.Status);

        }   
    }
}
