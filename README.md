Passo a passo de execução do projeto:

1- Instale o Visual Studio 2017

2- Instale o MongoDb

3- Instale o postman

4- Baixe o projeto

5- No diretório C:\Program Files\MongoDB\Server\3.6\bin executar o "mongod.exe"  para abrir a conexão do mongoDb para localhost:27017

6- Abra o projeto

7- Faça o Rebuild do projeto na Solution

No postman você deve utilizar as seguintes rotas iniciais:

1- Realizar uma requisição POST para http://localhost:{porta}/api/store com o objetivo de inserir o mock das lojas

2- Realizar uma requisição GET para http://localhost:{porta}/api/store com o objetivo de recuperar as lojas inseridas

3- Realizar uma requisição POST para http://localhost:{porta}/api/transaction com o objetivo de realizar uma transação
Obs: Os parâmetros devem ser enviados no Body da requisição

Exemplo de uma requisição:

Parâmetros do tipo JSON:

{

"IdStore": "348788333",
"NameStore": "Fake1",
"Type": "CreditCard",
"Amount": 102.00,
"Installments": 1,
"CardNumber" : "5415656465560",
"ExpMonth": "01",
"ExpYear": "2019",
"Brand": "MasterCard",
"Holder": "Testador",
"Cvv": "585",
"Email":"gateway@teste.com"

}

4- Realizar uma requisição GET para http://localhost:{porta}/api/transaction/id com o objetivo de recuperar as transações da loja