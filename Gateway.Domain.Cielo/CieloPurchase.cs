﻿using Gateway.Domain.Abstractions;
using Gateway.Domain.Cielo.Request;
using Gateway.Domain.Cielo.Converter;
using Gateway.Domain.Cielo.Response;
using Gateway.Domain.Services;
using System.Threading.Tasks;

namespace Gateway.Domain.Cielo
{
    public class CieloPurchase : IPurchaseTransaction<CreateTransactionRequest, TransactionResponse>
    {
        readonly IAntiFraudService _antiFraudService;

        public CieloPurchase(IAntiFraudService antiFraudService)
        {
            _antiFraudService = antiFraudService;            
        }

        public async Task<TransactionResponse> PayAsync(CreateTransactionRequest client)
        {            

            //Retornar Moke resposta
            var converter = new CieloTransactionResponseConverter();
            return converter.Convert(client);
            
        }
    }
}
