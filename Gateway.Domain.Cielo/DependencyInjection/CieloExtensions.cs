﻿using Autofac;

namespace Gateway.Domain.Cielo.DependencyInjection
{
    public static class CieloExtensions
    {
        public static void AddCieloGateway(this ContainerBuilder builder)
        {
            builder.RegisterType<CieloPurchase>().As<CieloPurchase>();
        }
    }
}
