﻿using Gateway.Domain.Abstractions;

namespace Gateway.Domain.Cielo.Response
{
    public class TransactionResponse : ICommandResult
    {
        public string MerchantOrderId { get; set; }

        public CustomerResponse Customer { get; set; }

        public PaymentResponse Payment { get; set; }

    }
}
