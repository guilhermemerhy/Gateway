﻿using System;

namespace Gateway.Domain.Cielo.Response
{
    public class PaymentResponse
    {
        public Guid PaymentId { get; set; }

        public string Type { get; set; }

        public byte Status { get; set; }

        public string ReturnCode { get; set; }

        public string ReturnMessage { get; set; }

        public int Installments { get; set; }

        public string ProofOfSale { get; set; }

        public CreditCardResponse CreditCard { get; set; }

        public double Amount { get; set; }

    }
}
