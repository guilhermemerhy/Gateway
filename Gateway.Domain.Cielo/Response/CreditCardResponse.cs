﻿namespace Gateway.Domain.Cielo.Response
{
    public class CreditCardResponse
    {
        public string CardNumber { get; set; }

        public string ExpirationDate { get; set; }

        public string Brand { get; set; }
        
    }
}
