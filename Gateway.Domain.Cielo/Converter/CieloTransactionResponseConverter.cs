﻿using Gateway.Domain.Abstractions;
using Gateway.Domain.Cielo.Request;
using Gateway.Domain.Cielo.Response;
using System;

namespace Gateway.Domain.Cielo.Converter
{
    public class CieloTransactionResponseConverter : IConverterObjects<CreateTransactionRequest, TransactionResponse>
    {
        public TransactionResponse Convert(CreateTransactionRequest from)
        {
            string[] code = { "4,05" };

            Random random = new Random();             
            int index = random.Next(code.Length);

            bool authorize = code[index] == "4";

            return new TransactionResponse
            {

                MerchantOrderId = from.MerchantOrderId,
                Customer = new CustomerResponse
                {
                    Name = from.Customer.Name
                },
                Payment = new PaymentResponse
                {
                    Installments = from.Payment.Installments,
                    Type = from.Payment.Type,
                    Amount = from.Payment.Amount,
                    PaymentId = Guid.NewGuid(),
                    ReturnCode = code[index],
                    ReturnMessage = authorize ? "Operation Successful" : "Unauthorized Operation",
                    Status = 2,
                    ProofOfSale = random.Next(0, 999999).ToString("D6"),

                    CreditCard = new CreditCardResponse
                    {
                        Brand = from.Payment.CreditCard.Brand,
                        CardNumber = from.Payment.CreditCard.CardNumber,
                        ExpirationDate = from.Payment.CreditCard.ExpirationDate,
                    }
                }

            };
        }
    }
}
