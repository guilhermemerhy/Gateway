﻿using Gateway.Domain.Abstractions;
using Gateway.Domain.Cielo.Request;
using System;

namespace Gateway.Domain.Cielo.Converters
{
    public class CieloCreateOrderRequestConverter : IConverterObjects<CreateOrderCommand, CreateTransactionRequest>
    {
        public CreateTransactionRequest Convert(CreateOrderCommand from)
        {
            return new CreateTransactionRequest
            {
                MerchantOrderId = new Random().Next(0, 999999).ToString("D6"),
                Customer = new CustomerRequest {

                    Name = from.NameStore,
                    Email = from.Email
                },
                Payment = new PaymentRequest
                {
                    Amount = from.Amount,
                    Installments = from.Installments,
                    Type = from.Type,
                    CreditCard = new CreditCardRequest
                    {
                        Brand = from.Brand,
                        CardNumber = from.CardNumber,
                        ExpirationDate = $"{from.ExpMonth}/{from.ExpYear}",
                        HolderName = from.Holder,
                        SecurityCode = from.Cvv
                    }
                }                
            };
        }
    }
}
