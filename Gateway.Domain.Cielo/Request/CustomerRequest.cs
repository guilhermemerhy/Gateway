﻿namespace Gateway.Domain.Cielo.Request
{
    public class CustomerRequest
    { 
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
