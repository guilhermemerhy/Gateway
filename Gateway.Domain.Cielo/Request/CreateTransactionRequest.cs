﻿using Gateway.Domain.Abstractions;

namespace Gateway.Domain.Cielo.Request
{
    public class CreateTransactionRequest : ICommand
    {    
        public string MerchantOrderId { get; set; }
        public CustomerRequest Customer { get; set; }
        public PaymentRequest Payment { get; set; }
    }
}
