﻿namespace Gateway.Domain.Cielo.Request
{
    public class PaymentRequest
    {
        public string Type { get; set; }

        public double Amount { get; set; }

        public int Installments { get; set; }

        public CreditCardRequest CreditCard { get; set; }
    }
}
