﻿namespace Gateway.Domain.Cielo.Request
{
    public class CreditCardRequest
    {

        public string CardNumber { get; set; }

        public string ExpirationDate { get; set; }

        public string Brand { get; set; }

        public string HolderName { get; set; }

        public string SecurityCode { get; set; }
    }
}
