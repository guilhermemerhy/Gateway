﻿using System.Threading.Tasks;

namespace Gateway.Domain.Abstractions
{
    public interface IPurchaseTransaction<TFrom, TTo>
        where TTo : ICommandResult
        where TFrom : ICommand

    {
        Task<TTo> PayAsync(TFrom client);
    }
}
