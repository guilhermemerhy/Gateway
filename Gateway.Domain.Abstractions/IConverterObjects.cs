﻿namespace Gateway.Domain.Abstractions
{
    public interface IConverterObjects<TFrom, TTo>
        where TTo : class 
        where TFrom : class
    {
        TTo Convert(TFrom from);
    }
}
