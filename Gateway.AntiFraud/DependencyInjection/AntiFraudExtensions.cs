﻿using Autofac;
using Gateway.AntiFraud.Services;
using Gateway.Domain.Services;

namespace Gateway.AntiFraud.DependencyInjection
{
    public static class AntiFraudExtensions
    {
        public static void AddAntiFraud(this ContainerBuilder builder)
        {            
            builder.RegisterType<AntiFraudService>().As<IAntiFraudService>();
        }
    }
}
