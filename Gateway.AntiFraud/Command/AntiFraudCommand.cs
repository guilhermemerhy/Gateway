﻿namespace Gateway.AntiFraud.Command
{
    public class AntiFraudCommand
    {
        public string ApiKey { get; set; }

        public string LoginToken { get; set; }

        public string AnalysisLocation { get; set; }

        public OrdersCommand Orders { get; set; }

    }
}
