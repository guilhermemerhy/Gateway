﻿using System;

namespace Gateway.AntiFraud.Command
{
    public class OrdersCommand
    {
        public string ID { get; set; }

        public DateTime Date { get; set; }

        public string Email { get; set; }

        public decimal TotalItems { get; set; }

        public decimal TotalOrder { get; set; }        
    }
}
