﻿namespace Gateway.AntiFraud.Response
{
    public class OrderStatusResponse
    {
        public string ID { get; set; }

        public string Status { get; set; }
        
    }
}
