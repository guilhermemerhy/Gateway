﻿namespace Gateway.AntiFraud.Response
{
    public class AntiFraudResponse
    {
        public string TransactionID { get; set; }

        public OrderStatusResponse Orders { get; set; }
    }
}
