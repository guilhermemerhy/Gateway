﻿using Gateway.AntiFraud.Command;
using Gateway.AntiFraud.Response;
using Gateway.Domain;
using Gateway.Domain.Enums;
using Gateway.Domain.Services;
using System;
using System.Threading.Tasks;

namespace Gateway.AntiFraud.Services
{
    public class AntiFraudService :  IAntiFraudService
    {

        private readonly NotificationContext _notificationContext;

        public AntiFraudService(NotificationContext notificationContext)
        {
            _notificationContext = notificationContext;
        }

        public async Task<bool> CheckAntiFraudAsync()
        {
            Random random = new Random();

            //Mokar dados para envio
            var antiFraud = new AntiFraudCommand
            {
                ApiKey = random.Next(0, 999999).ToString("D6"),
                LoginToken = random.Next(0, 999999).ToString("D6"),
                AnalysisLocation = "BRA",
                Orders = new OrdersCommand
                {
                    Date = DateTime.Now,
                    ID = random.Next(0, 999999).ToString("D6"),
                    Email = "teste@fake.com.br",
                    TotalItems = 10,
                    TotalOrder = 2
                }
            };


            var response = await ResponseAsync();

            if (response.Orders.Status != OrderStatus.APA.ToString())
                _notificationContext.AddNotification("AntiFraud", "Denied without prejudice – Order was denied with no indication of fraud. Usually due to impossibility of contact or invalid document.");


            return await Task.FromResult(!_notificationContext.HasNotifications);
        }

        //Mokar dados de resposta
        private async static Task<AntiFraudResponse> ResponseAsync()
        {
            var values = Enum.GetValues(typeof(OrderStatus));
            Random random = new Random();
            var randomBar = (OrderStatus)values.GetValue(random.Next(values.Length));

            return await Task.FromResult(new AntiFraudResponse
            {
                TransactionID = random.Next(0, 999999).ToString("D6"),
                Orders = new OrderStatusResponse
                {
                    ID = random.Next(0, 999999).ToString("D6"),
                    Status = randomBar.ToString()
                }
            });
        }
    }
}
